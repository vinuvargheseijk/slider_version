#phi = 1
# R- is a function of theta

from matplotlib.widgets import Slider, Button
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import sys
import argparse
from scipy import optimize
from matplotlib import font_manager
#font_manager.fontManager.addfont('/usr/local/share/texmf/fonts/truetype/public/marvosym/marvosym.ttf')
#No k/2R^2 term here
#log expansion
# minimal surface used here

kBT =  1.380649e-23 * 300
k = 63 * kBT
khat = 35 * kBT    # 35+- 7 k_BT       35 * 1.380649e-23 * 300    Joules
kentropy = 1 * kBT # The full term is kentropy * kBT*phitot * R^2, I'm
                        # merging in the kBT.
kad = 0.05e9
fm = 0.0e-6
#phitot = 1
Jaggr = 1.0e-5
Cp = 1/20e-9       # 100 nM radius for IRSP53 is a bit on the high side.
#Cp = 0.0       # 100 nM radius for IRSP53 is a bit on the high side.
xtot = 10e-6        # 10 micron dendritic segment.
dendDia = 1e-6
#ph = float(sys.argv[1])
phisat = 50e-18 # Saturation level: One molecule in 50 square nanometers
rpbounds= (0.5, 1.0)
rmbounds= (0.01, 500.0)
#tbounds= (0.0001, np.pi * 0.9999)
tbounds= (0.4, 0.45)

def entropy_eval(area, ph_en, khat, fs, k ):
    #Entropy is evaluated at a given value of ph_en
    entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) + (1 - ph_en) * np.log(1 - ph_en))
    return entropy
    

def entropy( area, phitot, khat, fs, k ):
    # Returns entropy per unit area
    ph_en = (phitot * phisat)/ area
    #entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) + (1-ph_en) * np.log(1-ph_en))
    #entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) - ph_en - 0.5 * ph_en**2 + ph_en**2 + 0.5 * ph_en**3)
    entropy_1_order = entropy_eval(area, 0.5, khat, fs, k )
    #entropy = entropy_1_order + 2 * ( (ph_en - 0.5)**2 + (2.0/3.0) * (ph_en - 0.5)**4 )
    entropy = 2 * ( (ph_en - 0.5)**2 + (2.0/3.0) * (ph_en - 0.5)**4 )
    return entropy

def aggregation_energy(rm, theta, phitot, k_agg, area):
    k_agg = k_agg * kBT
    ph_en = (phitot * phisat)/ area
    #return 1e-3 * area * (k_agg/phisat) * ph_en * (ph_en - 1.0)
    return area * (k_agg/area) * ph_en * (1.0  - ph_en)


def mismatch( rm, phitot, khat, fs, k ):
    # Note that the  area term cancels out with the phitot/(area)
    return 0.5 * khat * phitot * phisat * (1/rm - Cp) * (1/rm - Cp)

def adhesion( rp, theta ):
    r0 = (rp + rm) * np.sin( theta )
    ad = kad * np.pi * r0 * r0
    return ad

def fp_tension( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    return 2 * np.pi * rp * fs * (r0*theta + rp * ( np.cos( theta ) - 1 ) )

def fm_tension( rm, theta, phitot, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    return area * fs

def Fplus( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    ret = 2 * np.pi * (k * theta )
    return ret

def Fminus( rm, theta, phitot, khat, fs, k ):
    #Entropy is 0 for phi = 1
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fm = mismatch( rm, phitot, khat, fs, k) + area * ( k/(2*rm*rm))
    return Fm

def Fzero( rp, rm, theta, n, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    aDend = 2 * np.pi * dendDia * xtot - n * np.pi * r0 * r0
    f0 = aDend * ( fs + fm )
    return f0

def total_energy_wire(x,phi_per_spine, n, khat, fs, k):
    theta = x
    rm = np.sqrt( (phi_per_spine * phisat)/(2 * np.pi * (1 - np.cos(theta))) )
    rp = rm * np.sin(theta)
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fp = Fplus( rp, rm, theta, khat, fs, k )
    Fm = Fminus( rm, theta, phi_per_spine, khat, fs, k )
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )

    #total = F0 + n * ( Fm + Fp ) + fm_tension( rm, theta, phitot, khat, fs ) + fp_tension( rp, rm, theta, khat, fs )
    #total = F0 + n * ( Fm + Fp ) + n * fp_tension( rp, rm, theta, khat, fs ) + n * fm_tension( rm, theta, phitot, khat, fs )
    total = n * ( Fm + Fp ) + membrane_tension(x,phi_per_spine,n,khat,fs, k)
    # Note we are not including adhesion so far.
    return total * 1e16


def membrane_tension(x,phi_per_spine,n,khat,fs, k):
    theta = x
    rm = np.sqrt( (phi_per_spine * phisat)/(2 * np.pi * (1 - np.cos(theta))) )
    rp = rm * np.sin(theta)
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    fm_ten = n * fm_tension( rm, theta, phi_per_spine, khat, fs, k )
    fp_ten = n * fp_tension( rp, rm, theta, khat, fs, k )
    mem_ten = F0 + fm_ten + fp_ten
    baseline_tension = membrane_tension_no_spine(theta)
    return mem_ten-baseline_tension

def membrane_tension_no_spine(TL):
    aDend = 2 * np.pi * dendDia * xtot
    fs = sigma_v.val * 1e-7
    f0 = aDend * fs
    return [f0] * len(TL)


def update(val):
    TL = np.linspace(thmin_v.val,3.14,50) 
    khat = khat_v.val * 1.38e-23 * 300
    k = k_v.val * 1.38e-23 * 300
    fs = sigma_v.val * 1e-7
    phitot = phi_v.val
    if n_v.val !=0:
      phi_per_spine = phitot/n_v.val
    else:
      phi_per_spine = 0  
    ax.clear()
    ax1.clear()
    min_energy = 1e10
    for n in range( 1, 6 ):
        #Z=total_energy_wire(TL,phi_per_spine,n_v.val, khat, fs, k)
        if n == 0:
            Z = membrane_tension_no_spine(TL)
        else:    
            phi_per_spine = phitot/n
            Z=total_energy_wire(TL,phi_per_spine, n, khat, fs, k)
        local_min = np.min(Z)
        if n==1:
            print('Energy for n=1', local_min)
        if local_min < min_energy:
            min_energy = local_min
            opt_n = n
            print(opt_n)
            opt_theta_index = np.argmin(Z)
        if n == 0:
            ax.plot(TL, Z, color='b')
        else:    
            ax.plot(TL, Z, color='C'+str(n-1),label="#spines, N="+str(n))
    ax.legend()        
    print("printing new slected n for minimum energy")  
    print("min n, min energy",opt_n, min_energy)
    #print(Z)
    ax.set_xlabel("$\Theta$",fontsize=18)
    ax.set_ylabel("$Energy * 1e16$",fontsize=18)
    #opt_theta_index = np.argmin(Z)
    if opt_n == 0:
      opt_theta = 0  
    if opt_n != 0:
      opt_theta = ((3.14-thmin_v.val)/50) * opt_theta_index
      opt_theta = max( opt_theta, 1e-6 )
      opt_rm = np.sqrt( ((phitot/opt_n) * phisat)/(2 * np.pi * (1 - np.cos(opt_theta))) )
      area = 2 * np.pi * opt_rm**2 * (1 - np.cos(opt_theta))
      opt_phi = (phitot * phisat)/area
      opt_rm = opt_rm/1e-6
      rp = opt_rm * np.sin(opt_theta)
      x, y = dendShape(opt_theta , rp, opt_rm)
      ax1.plot(x,y)
      ax1.set_title("$R^{-}$="+str(round(opt_rm,2))+", $\Theta$="+str(round(opt_theta,3))+",$\phi$ = "+str(round(opt_phi,5)),fontsize=18)
      ax1.set_ylim(0, 0.8)
      ax1.set_xlim(4.5, 5.5)
    ax.set_title("N = {}, Min energy = {}".format( opt_n, round(min_energy,2)) )
    if opt_n == 0:
        col = 'b'
    else:    
        col = 'C' + str( opt_n - 1)
    ax.plot(opt_theta, min_energy, marker = r"$\odot$", color = col, markerfacecolor = col, markersize = '32')
    #print('Minim rm', opt_rm)


def dendShape( theta, rp, rm ):
    dth = theta / 50
    xbend = 2 * np.sin( theta ) * (rp + rm )
    Lflat = (xtot * 1e6 - xbend ) / 2.0
    x = [0.0, Lflat]
    y = [0.0, 0.0]
    if theta < 1e-3:
        return np.array( x ), np.array( y )
    # To show centre of curvature
    x.extend( [Lflat, Lflat] )
    y.extend( [rp, 0.0] )
    x.extend( [ Lflat + rp * np.sin( th + dth ) for th in np.arange(0.0, theta * 0.999999, dth ) ] )
    y.extend( [ rp * (1-np.cos( th + dth) ) for th in np.arange(0.0, theta * 0.999999, dth ) ] )
    xlast = x[-1]
    ylast = y[-1]
    xoffset = rm * np.sin( theta ) + xlast
    #xoffset = xtot / 2.0
    yoffset = -rm * np.cos(theta) + ylast
    #print('rm={:.4g} um, rp={:.4g} um, theta={:.4g} rad'.format( rm, rp, theta) )
    #print( xlast, ylast, xoffset, yoffset )
    x.append( xoffset )
    y.append( yoffset )

    x.extend( [ xoffset - rm * np.sin( th ) for th in np.arange (theta, 0, -dth ) ] )
    y.extend ([yoffset + rm * np.cos( th ) for th in np.arange (theta, 0, -dth ) ] )
    xlast = x[-1]
    ylast = y[-1]
    x.extend( [ xtot * 1e6 - i for i in x[::-1] ] )
    y.extend( y[::-1] )
    return np.array( x ), np.array( y )


def main():
    parser = argparse.ArgumentParser( description = "This routine finds energy minima for membrane deformation due to IBAR proteins" )
    parser.add_argument( "phitot", type = float, help = "Required: Total amount of protein, in number of molecules" )
    parser.add_argument( "-s", "--subset", type = str, help="Optional: Pick the subset of the energy term to display: F0, Fplus or Fminus", default = "" )
    parser.add_argument( "-fs", "--surfaceTension", type = float, help="Optional: energy for surface tension", default = 5.5e-6)
    parser.add_argument( "-t", "--tolerance", type = float, help="Optional: Tolerance for optimization", default = 1e-25)
    parser.add_argument( "-kh", "--khat", type = float, help="Optional: energy cost for mismatch with preferred curvature of IBAR, units kBT", default = 35.0)
    parser.add_argument( "-k", "--kcurve", type = float, help="Optional: energy cost for bare membrane curvature, units kBT", default = 63.0)
    parser.add_argument( "-ke", "--kentropy", type = float, help="Optional: energy term for entropy, units kBT", default = 1.0)
    parser.add_argument( "-theta", "--theta", type = float, help="Optional: Start value of theta, in radians.", default = 0.1)
    args = parser.parse_args()
    #global phitot
    #phitot = args.phitot
    #phtot= args.phitot * (1/150e-9)
    global k
    global kentropy
    fs = args.surfaceTension
    k = args.kcurve * kBT
    khat = args.khat * kBT
    kentropy = args.kentropy * kBT
    #global numSyn
    numSyn = 1
    global ax
    global ax1
    #global ax1
    fig = plt.figure(figsize=(12,8))
    plt.subplots_adjust(bottom = 0.45)
    ax = fig.add_subplot(1, 2, 1)

    global l
    global l1
    global l2
    global l3
    global RmL
    global X
    global Y
    global Z
    ax.set_xlabel("$R^{-} \mu m$")
    ax.set_ylabel("$\Theta$")
    ax1 = fig.add_subplot(1, 2, 2)
    ax_khat = plt.axes([0.25, 0.05, 0.65, 0.03])
    ax_k = plt.axes([0.25, 0.1, 0.65, 0.03])
    ax_phi = plt.axes([0.25, 0.15, 0.65, 0.03])
    ax_n = plt.axes([0.25, 0.2, 0.65, 0.03])
    ax_sigma = plt.axes([0.25, 0.25, 0.65, 0.03])
    ax_thmin = plt.axes([0.25, 0.3, 0.65, 0.03])
    global sigma_v
    global khat_v
    #global theta_v
    global k_v
    global phi_v
    global n_v
    global thmin_v
    sigma_v = Slider(ax_sigma, '$\sigma * 1e-7$', 0.0, 1000, 55)
    sigma_v.label.set_size(18)
    khat_v = Slider(ax_khat, '$\hat{k} * KBT$', 10, 40, 35, valstep = 5)
    khat_v.label.set_size(18)
    k_v = Slider(ax_k, '$k * KBT$', 10, 100, 63, valstep = 5)
    k_v.label.set_size(18)
    phi_v = Slider(ax_phi, '$\phi_{tot}$', 1, 1e4, 100, valstep = 50)
    phi_v.label.set_size(18)
    #theta_v = Slider(ax_theta, '$\Theta$', 0.001, 3.14, 1)
    n_v = Slider(ax_n, '$n$', 0, 10, 1, valstep = 1)
    n_v.label.set_size(18)
    thmin_v = Slider(ax_thmin, '$\Theta_{min}$', 0.001, 1, 0.001, valstep = 0.1)
    thmin_v.label.set_size(18)
    sigma_v.on_changed(update)
    khat_v.on_changed(update)
    #theta_v.on_changed(update)
    phi_v.on_changed(update)
    k_v.on_changed(update)
    n_v.on_changed(update)
    thmin_v.on_changed(update)
    plt.show()



if __name__ == '__main__':
    main()
    


