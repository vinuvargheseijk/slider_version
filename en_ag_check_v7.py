import numpy as np
import matplotlib.pyplot as plt


ph=np.linspace(0.01,0.99,100)
phitot = 1000
phisat = 1/50e-18
en = []
agg = []
area = []
sum_e=[]
dendDia = 1e-6
xtot = 10e-6
#base_area = 2 * np.pi * xtot * 0.5 * dendDia
temp_area = []
base_area = 0.0
C_aggr = 1
coeff = -2
k_agg=-36
kentropy = 1.038e-23*300
#base_phi = phitot/(phisat*base_area)
#print('Base area, phi', base_area, base_phi)
for i in range(len(ph)):
    if ph[i]==0.0:
        print(ph[i])
        area.append(base_area)
        temp_area.append(0)
        en.append(0)
        agg.append(0)
    elif ph[i]==1:
        print(ph[i])
        area.append(base_area + phitot/(ph[i]*phisat))
        temp_area.append(phitot/(ph[i]*phisat))
        en.append(area[i] * (1.038e-23*300)*phisat * ph[i] * (np.log(ph[i]) - 1))
        #en.append(0)
        #agg.append(-20* area[i] * (1.038e-23*300)*phisat * ph[i] * (1 - ph[i]))
        agg.append(area[i] * (kentropy*phisat) * ( 0.5 * k_agg * ph[i]**2 + 5 * ph[i]**3 + 6.12 * ph[i]**4 + 7.06 * ph[i]**5))
    else:    
        area.append(base_area + phitot/(ph[i]*phisat))
        temp_area.append(phitot/(ph[i]*phisat))
        en.append(area[i] * (1.038e-23*300)*phisat * ph[i] * (np.log(ph[i]) - 1))
        agg.append(area[i] * (kentropy*phisat) * ( 0.5 * k_agg * ph[i]**2 + 5 * ph[i]**3 + 6.12 * ph[i]**4 + 7.06 * ph[i]**5))
              
                
for i in range(len(en)):
    sum_e.append(en[i]+agg[i])

print('Minimum of energy at',sum_e.index(min(sum_e)),'value',ph[sum_e.index(min(sum_e))])
plt.plot(ph,en,label='Entropy')
plt.plot(ph,agg,label='Aggregation')
plt.plot(ph,sum_e,label='Entopy + Aggregation')
plt.xlabel('$\phi$',fontsize=18)
plt.legend()
plt.figure(101)
plt.plot(ph,area,label='Area $m^{2}$')
plt.plot(ph,temp_area,label='Area $m^{2}$')
plt.xlabel('$\phi$',fontsize=18)
plt.legend()
plt.show()





