from matplotlib.widgets import Slider, Button
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import sys
import argparse
from scipy import optimize
import cProfile
#No k/2R^2 term here
#log expansion
# minimal surface used here

kBT =  1.380649e-23 * 300
k = 63 * kBT
khat = 35 * kBT    # 35+- 7 k_BT       35 * 1.380649e-23 * 300    Joules
kentropy = 1 * kBT # The full term is kentropy * kBT*phitot * R^2, I'm
                        # merging in the kBT.
kad = 0.05e9
fm = 0.0e-6
#phitot = 1
Jaggr = 1.0e-5
Cp = 1/20e-9       # 100 nM radius for IRSP53 is a bit on the high side.
#Cp = 0.0       # 100 nM radius for IRSP53 is a bit on the high side.
xtot = 10e-6        # 10 micron dendritic segment.
dendDia = 1e-6
#ph = float(sys.argv[1])
#phisat = 50e-18 # Saturation level: One molecule in 50 square nanometers
rpbounds= (0.5, 1.0)
rmbounds= (0.01, 500.0)
#tbounds= (0.0001, np.pi * 0.9999)
tbounds= (0.4, 0.45)


def entropy( area, phitot, phisat, khat, fs, k ):
    # Returns entropy per unit area
    ph_en = (phitot * phisat)/ area
    #entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) + (1-ph_en) * np.log(1-ph_en))
    entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) - ph_en - 0.5 * ph_en**2 + ph_en**2 + 0.5 * ph_en**3)
    return entropy

def aggregation_energy(rm, theta, phitot, phisat, k_agg, area):
    ph_en = (phitot * phisat)/ area
    return area * (kentropy/phisat) * ( 0.5 * k_agg * ph_en**2 + 5 * ph_en**3 + 6.12 * ph_en**4 + 7.06 * ph_en**5)
    #return 1e-3 * area * (k_agg/phisat) * (ph_en - 1.0)


def mismatch( rm, phitot, phisat, khat, fs, k ):
    # Note that the  area term cancels out with the phitot/(area)
    return 0.5 * khat * phitot * phisat * (1/rm - Cp) * (1/rm - Cp)

def adhesion( rp, theta ):
    r0 = (rp + rm) * np.sin( theta )
    ad = kad * np.pi * r0 * r0
    return ad

def fp_tension( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    return 2 * np.pi * rp * fs * (r0*theta + rp * ( np.cos( theta ) - 1 ) )

def fm_tension( rm, theta, phitot, phisat, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    return area * fs

def Fplus( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    ret = 2 * np.pi * (k * theta )
    return ret

def Fminus( rm, theta, phitot, phisat, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fm = mismatch( rm, phitot, phisat, khat, fs, k) + area * ( k/(2*rm*rm) + entropy( area, phitot, phisat, khat, fs, k ) )
    return Fm

def Fzero( rp, rm, theta, n, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    aDend = 2 * np.pi * dendDia * xtot - n * np.pi * r0 * r0
    f0 = aDend * ( fs + fm )
    return f0

def total_energy_wire(x,y,phitot, phisat, n, khat, fs, k, k_ag_en):
    rm = x * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fp = Fplus( rp, rm, theta, khat, fs, k )
    Fm = Fminus( rm, theta, phitot, phisat, khat, fs, k )
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    agg_en = aggregation_energy(rm, theta, phitot, phisat, k_ag_en, area)

    #total = F0 + n * ( Fm + Fp ) + fm_tension( rm, theta, phitot, khat, fs ) + fp_tension( rp, rm, theta, khat, fs )
    #total = F0 + n * ( Fm + Fp ) + n * fp_tension( rp, rm, theta, khat, fs ) + n * fm_tension( rm, theta, phitot, khat, fs )
    basal_tension_energy = ( fs + fm ) * 2 * np.pi * dendDia * xtot
    total = n * ( Fm + Fp ) + membrane_tension(x,y,phisat,phitot,n,khat,fs, k) + n * agg_en - basal_tension_energy
    # Note we are not including adhesion so far.
    return total * 1e16


def total_energy(x,phitot, phisat, n, khat, fs, k, k_ag_en):
    rm = x[0] * 1e-6
    theta = x[1]
    rp = rm * np.sin(theta)
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fp = Fplus( rp, rm, theta, khat, fs, k )
    Fm = Fminus( rm, theta, phitot, phisat, khat, fs, k )
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    agg_en = aggregation_energy(rm, theta, phitot, phisat, k_ag_en, area)

    #total = F0 + n * ( Fm + Fp ) + fm_tension( rm, theta, phitot, khat, fs ) + fp_tension( rp, rm, theta, khat, fs )
    #total = F0 + n * ( Fm + Fp ) + n * fp_tension( rp, rm, theta, khat, fs ) + n * fm_tension( rm, theta, phitot, khat, fs )
    basal_tension_energy = ( fs + fm ) * 2 * np.pi * dendDia * xtot
    total = n * ( Fm + Fp ) + membrane_tension(rm,theta,phisat,phitot,n,khat,fs, k) + n * agg_en - basal_tension_energy
    # Note we are not including adhesion so far.
    return total * 1e16 

def membrane_tension(x,y,phitot,phisat,n,khat,fs, k):
    rm = x * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    fm_ten = n * fm_tension( rm, theta, phitot, phisat, khat, fs, k )
    fp_ten = n * fp_tension( rp, rm, theta, khat, fs, k )
    mem_ten = F0 + fm_ten + fp_ten
    return mem_ten

def bending(x,y,phitot,n,khat,fs):
    rm = np.asarray(x) * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    area_m = np.pi * rm * rm * ( 1 - np.cos( theta ) )
    r0 = (rp + rm) * np.sin( theta )
    fp_bend = 2 * np.pi * k * theta
    fm_bend = area_m *  k/(2*rm*rm)
    mem_bend = fp_bend + fm_bend
    return mem_bend * 1e16

def update(val):
    x0 = [0.5, 0.5] 
    rmbounds = (0.01,100.0)
    tbounds = (0.001,3.14)
    TL = np.linspace(0.001,3.14,100) 
    khat = khat_v.val * 1.38e-23 * 300
    k = k_v.val * 1.38e-23 * 300
    fs = sigma_v.val * 1e-7
    phitot = phi_v.val/n_v.val
    ax.clear()
    ax1.clear()
    phisat = phisat_v.val * 1e-18

    basal_tension_energy = ( fs + fm ) * 2 * np.pi * dendDia * xtot * 1e16
    ret = optimize.minimize( total_energy, x0=x0, bounds = (rmbounds,tbounds),args=(phitot,phisat,n_v.val, khat, fs, k, ag_en_v.val),method = 'L-BFGS-B' )
    print("optimum",ret)
    #Z=total_energy_wire(X,Y,phitot,phisat,n_v.val, khat, fs, k, ag_en_v.val) - basal_tension_energy
    #temp = Z
    #temp_v = np.where(temp==Z.min())
    #i1 = temp_v[0][0]
    #i2 = temp_v[1][0]
    #minim_ph = (phitot*phisat)/(2 * np.pi * (X[i1][i2]*1e-6)**2 * (1 - np.cos(Y[i1][i2]))) 
    rm = ret.x[0]
    theta = ret.x[1]
    minim_ph = (phitot*phisat)/(2 * np.pi * (rm*1e-6)**2 * (1 - np.cos(theta))) 
    par = [rm, TL]
    twod_section = total_energy(par, phitot,phisat,n_v.val, khat, fs, k, ag_en_v.val)
    ax.plot(TL, twod_section)
    ax.set_title("Min Energy"+str(ret.fun))
    ax.set_xlabel("$\Theta$",fontsize=18)
    ax.set_ylabel("$Energy$",fontsize=18)
    ax.set_ylim(min(twod_section),5)
    rp = rm * np.sin(theta)
    x, y = dendShape( theta, rp, rm)
    ax1.plot(x,y)
    print('rm,theta',rm,theta)
    ax1.set_title("$R^{d}$="+str(round(rm,2))+", $\Theta$="+str(round(theta,2))+", $\phi$ ="+str(round(minim_ph,5)),fontsize=18)
    ax1.set_ylim(0,max(y))
    ax1.set_xlim(4,6)


def dendShape( theta, rp, rm ):
    dth = theta / 50
    xbend = 2 * np.sin( theta ) * (rp + rm )
    Lflat = (xtot * 1e6 - xbend ) / 2.0
    x = [0.0, Lflat]
    y = [0.0, 0.0]
    # To show centre of curvature
    x.extend( [Lflat, Lflat] )
    y.extend( [rp, 0.0] )
    x.extend( [ Lflat + rp * np.sin( th + dth ) for th in np.arange(0.0, theta * 0.999999, dth ) ] )
    y.extend( [ rp * (1-np.cos( th + dth) ) for th in np.arange(0.0, theta * 0.999999, dth ) ] )
    xlast = x[-1]
    ylast = y[-1]
    xoffset = rm * np.sin( theta ) + xlast
    #xoffset = xtot / 2.0
    yoffset = -rm * np.cos(theta) + ylast
    #print('rm={:.4g} um, rp={:.4g} um, theta={:.4g} rad'.format( rm, rp, theta) )
    #print( xlast, ylast, xoffset, yoffset )
    x.append( xoffset )
    y.append( yoffset )

    x.extend( [ xoffset - rm * np.sin( th ) for th in np.arange (theta, 0, -dth ) ] )
    y.extend ([yoffset + rm * np.cos( th ) for th in np.arange (theta, 0, -dth ) ] )
    xlast = x[-1]
    ylast = y[-1]
    x.extend( [ xtot * 1e6 - i for i in x[::-1] ] )
    y.extend( y[::-1] )
    return np.array( x ), np.array( y )


def main():
    parser = argparse.ArgumentParser( description = "This routine finds energy minima for membrane deformation due to IBAR proteins" )
    parser.add_argument( "phitot", type = float, help = "Required: Total amount of protein, in number of molecules" )
    parser.add_argument( "-s", "--subset", type = str, help="Optional: Pick the subset of the energy term to display: F0, Fplus or Fminus", default = "" )
    parser.add_argument( "-fs", "--surfaceTension", type = float, help="Optional: energy for surface tension", default = 5.5e-6)
    parser.add_argument( "-t", "--tolerance", type = float, help="Optional: Tolerance for optimization", default = 1e-25)
    parser.add_argument( "-kh", "--khat", type = float, help="Optional: energy cost for mismatch with preferred curvature of IBAR, units kBT", default = 35.0)
    parser.add_argument( "-k", "--kcurve", type = float, help="Optional: energy cost for bare membrane curvature, units kBT", default = 63.0)
    parser.add_argument( "-ke", "--kentropy", type = float, help="Optional: energy term for entropy, units kBT", default = 1.0)
    parser.add_argument( "-theta", "--theta", type = float, help="Optional: Start value of theta, in radians.", default = 0.1)
    args = parser.parse_args()
    #global phitot
    #phitot = args.phitot
    #phtot= args.phitot * (1/150e-9)
    global k
    global kentropy
    fs = args.surfaceTension
    k = args.kcurve * kBT
    khat = args.khat * kBT
    kentropy = args.kentropy * kBT
    #global numSyn
    numSyn = 1
    global ax
    global ax1
    #global ax1
    fig = plt.figure(figsize=(12,8))
    plt.subplots_adjust(bottom = 0.45)
    #ax = fig.add_subplot(1, 2, 1, projection='3d')
    ax = fig.add_subplot(1, 2, 1)

    global l
    global l1
    global l2
    global l3
    global RmL
    global X
    global Y
    global Z
    ax.set_xlabel("$\Theta$")
    ax.set_ylabel("$Energy$")
    ax1 = fig.add_subplot(1, 2, 2)
    ax_khat = plt.axes([0.25, 0.05, 0.65, 0.03])
    ax_k = plt.axes([0.25, 0.1, 0.65, 0.03])
    ax_phi = plt.axes([0.25, 0.15, 0.65, 0.03])
    ax_n = plt.axes([0.25, 0.2, 0.65, 0.03])
    ax_sigma = plt.axes([0.25, 0.25, 0.65, 0.03])
    ax_phisat = plt.axes([0.25, 0.3, 0.65, 0.03])
    ax_ag_en = plt.axes([0.25, 0.35, 0.65, 0.03])
    global sigma_v
    global khat_v
    #global theta_v
    global k_v
    global phi_v
    global n_v
    global phisat_v
    global ag_en_v
    sigma_v = Slider(ax_sigma, '$\sigma * 1e-7$', 0.0, 1000, 55)
    sigma_v.label.set_size(18)
    khat_v = Slider(ax_khat, '$\hat{k} * KBT$', 10, 40, 35, valstep = 5)
    khat_v.label.set_size(18)
    k_v = Slider(ax_k, '$k * KBT$', 10, 100, 63, valstep = 5)
    k_v.label.set_size(18)
    phi_v = Slider(ax_phi, '$\phi_{tot}$', 1, 1e4, 100, valstep = 50)
    phi_v.label.set_size(18)
    ax_ag_en.set_title("$(Ag. Energy. = (K_{B}T)*\phi_{sat} (0.5 * a * \phi^{2} + 5 * \phi^{3} + 6.12 * \phi^{4} + 7.06 * \phi^{5})$",fontsize=18)
    ag_en_v = Slider(ax_ag_en, '$a = $', -100, 100, -30)
    ag_en_v.label.set_size(18)
    #theta_v = Slider(ax_theta, '$\Theta$', 0.001, 3.14, 1)
    n_v = Slider(ax_n, '$n$', 1, 5, 1, valstep = 1)
    n_v.label.set_size(18)
    phisat_v = Slider(ax_phisat, '$\phi_{sat}*1e18$', 10, 200, 50)
    phisat_v.label.set_size(18)
    sigma_v.on_changed(update)
    khat_v.on_changed(update)
    ag_en_v.on_changed(update)
    #theta_v.on_changed(update)
    phi_v.on_changed(update)
    k_v.on_changed(update)
    n_v.on_changed(update)
    phisat_v.on_changed(update)
    plt.show()



if __name__ == '__main__':
    main()
    


